import config from './src/config/config.js';
import app from './src//config/express.js';

app.listen(config.port, () => {
    console.log('server started on port 3000');
});

export default app;