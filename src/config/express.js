import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import index from '../server/routes/index.route.js';
import exchange from '../server/routes/exchange_rate.route.js';
import userRoute from '../server/routes/user.route.js';

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

app.get('/', (req, res) => {
    res.send('server is started with restful call');
});

app.use('/api', index);
app.use('/xerate', exchange);
app.use('/user', userRoute);

export default app;