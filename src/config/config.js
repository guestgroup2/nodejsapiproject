const config = {
    version: '1.0.0',
    env: 'development',
    port: '5000'
};

export default config;