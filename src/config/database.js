const db = {
    development: {
        username: '',
        password: '',
        database: 'user_management',
        host: 'localhost',
        dialect: 'mysql',
        port: '3306'
    }
};

export default db;
