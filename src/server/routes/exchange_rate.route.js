import express from 'express';
import config from './../../config/config.js';
import exchangeRateCtrl from '../controllers/exchange_rate.controller.js'
const router = express.Router();

router.route('/').get(exchangeRateCtrl.hkUkExrateGet);
router.route('/symbols').get(exchangeRateCtrl.hkUkSymbolsGet);

export default router;