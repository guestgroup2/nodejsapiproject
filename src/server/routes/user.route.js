import express from 'express';
//import config from './../../config/config.js';
import userCtrl from '../controllers/user.controller.js'
const router = express.Router();

router.route('/').get(userCtrl.allUserGet);
router.route('/reg').post(userCtrl.userRegistrationPost)

export default router;