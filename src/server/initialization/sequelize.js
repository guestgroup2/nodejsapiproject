import { Sequelize } from "sequelize";
import db from '../../../src/config/database.js';

const sequelize = new Sequelize(db.development.database, db.development.username, db.development.password, {
    host: db.development.host,
    port: db.development.port,
    dialect: db.development.dialect
});

sequelize.sync();

export default sequelize;