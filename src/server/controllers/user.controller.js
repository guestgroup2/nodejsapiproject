import userModule from '../modules/user.module.js';

const allUserGet = async (req, res) => {
    var result = await userModule.getAllUsers();
    res.send(result);
};

const userRegistrationPost = async (req, res) => {
    console.log(req.body);
    var result = await userModule.userRegister(req.body);
    res.send({success: true});
};

export default {
    allUserGet,
    userRegistrationPost
};