import exchangeRate from '../modules/exchange_rate.module.js';

const hkUkExrateGet = async (req, res) => {
    var result = await exchangeRate.getHkUkExRate().catch((err) => {
        return res.send(err);
    });
    res.send(result);
};

const hkUkSymbolsGet = async (req, res) => {
    var result = await exchangeRate.getHkUkSymbols().catch((err) => {
        return res.send(err);
    });
    res.send(result);
};

export default {
    hkUkExrateGet,
    hkUkSymbolsGet
};
