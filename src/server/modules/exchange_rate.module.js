import axios from 'axios';

const getHkUkExRate = async () => {
    var params = {
        params: {
            access_key: '',
            base: 'EUR',
            symbols:'GBP'

        }
    }
    var result = await axios.get('http://data.fixer.io/api/latest', params);
    console.log(JSON.stringify(result.data));
    return result.data;
};

const getHkUkSymbols = async () => {
    var params = {
        params: {
            access_key: ''

        }
    }
    var result = await axios.get('http://data.fixer.io/api/symbols', params);
    console.log(JSON.stringify(result.data));
    return result.data;
};

export default {
    getHkUkExRate,
    getHkUkSymbols
};
