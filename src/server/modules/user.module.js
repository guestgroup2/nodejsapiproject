//import axios from 'axios';
import sequelize from '../initialization/sequelize.js';
import usr from '../models/User.js'

const getAllUsers = async () => {
    var user = usr(sequelize);
    const res = await user.findAll()
    /*.then(users => {
        return users;
    });*/
    console.log(res);
    return res;
};

const userRegister = async (formValues) => {
    var user = usr(sequelize);
    const res = await user.create({
        first_name: formValues.firstname,
        last_name: formValues.lastname,
        email: formValues.email
    })
    /*.then(users => {
        return users;
    });*/
    console.log(res);
    return res;
};

export default {
    getAllUsers,
    userRegister
};