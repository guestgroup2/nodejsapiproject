import { Model, Sequelize } from 'sequelize';

const usr = (sequelize) => {
    class User extends Model {
        /*static associate(models) {

        }*/
    };
    User.init({
        id: {
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        first_name: Sequelize.STRING,
        last_name: Sequelize.STRING,
        email: Sequelize.STRING,
    }, {
        sequelize,
        modelName: 'users',
        timestamps: false
    });
    return User;
}

export default usr;